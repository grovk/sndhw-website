#!/bin/bash

tar -cvf ./site.release-1.2.3.gz /var/www/html
git checkout archives
git add .
git commit -am "tarsite from release 1.2.3"
git push -u origin
git checkout master
